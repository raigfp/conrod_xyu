#[macro_use] extern crate conrod;
mod support;
extern crate find_folder;
extern crate piston_window;
extern crate piston;
extern crate opengl_graphics;
extern crate gfx_device_gl;

use self::piston_window::{PistonWindow, Window, WindowSettings, AdvancedWindow};
use self::piston_window::{Input, Motion, UpdateArgs, RenderArgs};
use self::piston_window::{G2d, G2dTexture, TextureSettings};
use self::piston_window::OpenGL;
use self::piston_window::texture::UpdateTexture;
use self::piston_window::{clear, rectangle};

use conrod::backend::piston::draw::Transformed;
use conrod::{widget, Widget, Colorable, color};
use conrod::text::GlyphCache;

pub struct App {
    rotation: f64
}

impl App {
    fn render(&mut self, window: &mut PistonWindow, event: &Input, args: RenderArgs) {
        let (x, y) = (
            (args.width / 2) as f64,
            (args.height / 2) as f64
        );
        let width = 50.0;
        let half_width = width / 2.0;

        window.draw_2d(event, |context, graphics| {
            let transform = context.transform
                .trans(x, y)
                .rot_rad(self.rotation)
                .trans(-half_width, -half_width);

            clear([32.0 / 255.0, 74.0 / 255.0, 135.0 / 255.0, 1.0], graphics);
            rectangle(
                [114.0 / 255.0, 159.0 / 255.0, 207.0 / 255.0, 1.0],
                [0.0, 0.0, width, width],
                transform,
                graphics
            );
        });
    }

    fn update(&mut self, args: UpdateArgs) {
        self.rotation += 2.0 * args.dt;
    }
}

fn main() {
    const WIDTH: u32 = 1024;
    const HEIGHT: u32 = 768;
    const HALF_WIDTH: f64 = WIDTH as f64 / 2.0;
    const HALF_HEIGHT: f64 = HEIGHT as f64 / 2.0;
    let opengl = OpenGL::V3_2;

    // Construct the window.
    let mut window: PistonWindow =
        WindowSettings::new("All Widgets - Piston Backend", [WIDTH, HEIGHT])
            .opengl(opengl)
            .samples(4)
            .exit_on_esc(true)
            .vsync(true)
            .build()
            .unwrap();

    window.set_capture_cursor(true);

    // construct our `Ui`.
    let mut ui = conrod::UiBuilder::new([WIDTH as f64, HEIGHT as f64])
        .theme(support::theme())
        .build();

    // Add a `Font` to the `Ui`'s `font::Map` from file.
    let assets = find_folder::Search::KidsThenParents(3, 5).for_folder("assets").unwrap();
    let font_path = assets.join("fonts/NotoSans/NotoSans-Regular.ttf");
    ui.fonts.insert_from_file(font_path).unwrap();

    // Create a texture to use for efficiently caching text on the GPU.
    let mut text_vertex_data = Vec::new();
    let (mut glyph_cache, mut text_texture_cache) = {
        const SCALE_TOLERANCE: f32 = 0.1;
        const POSITION_TOLERANCE: f32 = 0.1;
        let cache = GlyphCache::new(WIDTH, HEIGHT, SCALE_TOLERANCE, POSITION_TOLERANCE);
        let buffer_len = WIDTH as usize * HEIGHT as usize;
        let init = vec![128; buffer_len];
        let settings = TextureSettings::new();
        let factory = &mut window.factory;
        let texture = G2dTexture::from_memory_alpha(factory, &init, WIDTH, HEIGHT, &settings).unwrap();
        (cache, texture)
    };

    // Instantiate the generated list of widget identifiers.
    let ids = support::Ids::new(ui.widget_id_generator());

    // Create our `conrod::image::Map` which describes each of our widget->image mappings.
    // In our case we only have one image, however the macro may be used to list multiple.
    let image_map = conrod::image::Map::new();

    let mut cursor_x = HALF_WIDTH;
    let mut cursor_y = HALF_HEIGHT;

    let mut app = App {
        rotation: 0.0
    };

    // Poll events from the window.
    while let Some(event) = window.next() {
        match event {
            Input::Move(Motion::MouseRelative(x, y)) => {
                cursor_x += x;
                cursor_y += y;
                ui.handle_event(conrod::event::Input::Move(
                    Motion::MouseCursor(
                        cursor_x - HALF_WIDTH,
                        -cursor_y + HALF_HEIGHT
                    )
                ))
            }

            Input::Render(args) => {
                app.render(&mut window, &event, args);

                window.draw_2d(&event, |context, graphics| {
                    // conrod
                    // A function used for caching glyphs to the texture cache.
                    let cache_queued_glyphs = |graphics: &mut G2d,
                                                cache: &mut G2dTexture,
                                                rect: conrod::text::rt::Rect<u32>,
                                                data: &[u8]|
                    {
                        let offset = [rect.min.x, rect.min.y];
                        let size = [rect.width(), rect.height()];
                        let format = piston_window::texture::Format::Rgba8;
                        let encoder = &mut graphics.encoder;
                        text_vertex_data.clear();

                        text_vertex_data.extend(data.iter().flat_map(|&b| vec![255, 255, 255, b]));
                        UpdateTexture::update(cache, encoder, format, &text_vertex_data[..], offset, size)
                            .expect("failed to update texture")
                    };

                    // Specify how to get the drawable texture from the image. In this case, the image
                    // *is* the texture.
                    fn texture_from_image<T>(img: &T) -> &T { img }

                    // Draw the conrod `render::Primitives`.
                    conrod::backend::piston::draw::primitives(
                        ui.draw(),
                        context,
                        graphics,
                        &mut text_texture_cache,
                        &mut glyph_cache,
                        &image_map,
                        cache_queued_glyphs,
                        texture_from_image
                    );

                    rectangle(
                        [1.0, 1.0, 1.0, 1.0],
                        [-5.0, -5.0, 10.0, 10.0],
                        context.transform.trans(cursor_x, cursor_y),
                        graphics
                    );
                });
            }

            Input::Update(args) => {
                app.update(args);

                let mut ui = ui.set_widgets();
                widget::Canvas::new().flow_down(&[
                    (ids.header, widget::Canvas::new().length(50.0).color(color::CHARCOAL)),
                    (ids.body, widget::Canvas::new()),
                    (ids.footer, widget::Canvas::new().length(300.0).color(color::CHARCOAL)),
                ]).set(ids.master, &mut ui);
            }

            _ => { }
        }

        // Convert the piston event to a conrod event.
        let size = window.size();
        let (win_w, win_h) = (size.width as conrod::Scalar, size.height as conrod::Scalar);
        if let Some(e) = conrod::backend::piston::event::convert(event.clone(), win_w, win_h) {
            ui.handle_event(e);
        }
    }
}
